'use strict';
exports.__esModule = true;
// require('es6-shim');
// require('reflect-metadata');
var path = require('path');
var bodyParser = require('body-parser');
var express = require('express');
var allowedExt = [
  '.js',
  '.ico',
  '.css',
  '.png',
  '.jpg',
  '.woff2',
  '.woff',
  '.ttf',
  '.svg',
  '.webmanifest',
  '.map',
  '.json',
];
var Server = (function() {
  function Server() {
    var _this = this;
    this.port = 4200;
    // Create expressjs application
    this.app = express();
    //Route our backend calls
    this.app.get('/api', function(req, res) {
      return res.json({application: 'Reibo collection'});
    });
    this.app.get('/ngsw.json?*',function(reqc,res){
        res.sendFile(path.resolve('dist/asset-invoice-tracking/ngsw.json'));
    })
    this.app.get('*', function(req, res) {
      console.log(req.url)
      if (
        allowedExt.filter(function(ext) {
          return req.url.indexOf(ext) > 0;
        }).length > 0
      ) {
        res.sendFile(path.resolve('dist/asset-invoice-tracking/' + req.url));
      } else {
        res.sendFile(path.resolve('dist/asset-invoice-tracking/index.html'));
      }
    });
    this.app.use(bodyParser.json({limit: '50mb'}));
    this.app.use(bodyParser.raw({limit: '50mb'}));
    this.app.use(bodyParser.text({limit: '50mb'}));
    this.app.use(
      bodyParser.urlencoded({
        limit: '50mb',
        extended: true,
      })
    );
    this.app.listen(this.port, function() {
      return console.log('http is started ' + _this.port);
    });
    this.app.on('error', function(error) {
      console.error('ERROR', error);
    });
    process.on('uncaughtException', function(error) {
      console.log(error);
    });
  }
  Server.bootstrap = function() {
    return new Server();
  };
  return Server;
})();
//Bootstrap the server, so it is actualy started
var server = Server.bootstrap();
exports['default'] = server.app;
