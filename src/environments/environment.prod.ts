export const environment = {
  production: true,
  apiBase: {
    EndCustomer: [
      'https://pacific-journey-95032.herokuapp.com/api/v1/',
      'https://pacific-journey-95032.herokuapp.com/api/v1/'
    ],
    LeasingPartner: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    SupplyChainTeam: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    ManufacturingUnit: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    InstallationPartner: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
    default: 'https://pacific-journey-95032.herokuapp.com/api/v1/'
  },
  auth: {
    URLS: {
      base: 'https://pacific-journey-95032.herokuapp.com/api/v1/',
      login: 'oauth/authorize',
      token: 'oauth/token',
      logout: 'oauth/logout',
      me: 'scim/v2/Me',
    },
    clientId: 'dscfzami9szhhnxB8INsLsivwPjIzVHJ',
    clientSecret: 'secret',
    redirectURI: 'http://localhost:4200/login',
    logoutRedirectURI: 'http://localhost:4200/login'
  },
  users: [
    {
      userName: 'ashish@gmail.com',
      role: 'EndCustomer',
      nodeIndex: 0,
      customerorgname: 'customer1',
      signatoryposition: 'CEO'
    },
    {
      userName: 'oven@gmail.com',
      role: 'EndCustomer',
      nodeIndex: 2,
      customerorgname: 'customer1',
      signatoryposition: 'IT'
    },
    {
      userName: 'jim@gmail.com',
      role: 'LeasingPartner',
      customerorgname: 'customer2',
      signatoryposition: 'chairman'
    },
    {
      userName: 'sesse@gmail.com',
      role: 'SupplyChainTeam',
      customerorgname: 'customer3',
      signatoryposition: 'IT'
    },
    {
      userName: 'gaurav@gmail.com',
      role: 'InstallationPartner',
      signatoryposition: 'programmer'
    },
    {
      userName: 'mahesh@gmail.com',
      role: 'ManufacturingUnit',
      signatoryposition: 'manager'
    }
  ]
};
