import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { LocalStorageConfig } from 'src/app/configs/local-storage.config';
import { environment } from 'src/environments/environment';
import { User } from '../models/user.model';
import { AvatarConfig } from 'src/app/configs/avatar.config';

import { ToastService } from '../services/toast.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private currentUser: User;

  constructor(
    private router: Router,
    private http: HttpClient,
    private toastService: ToastService
  ) { }

  /**
   * fetch access token given the grant code
   * once successfully fetched, save the token to localstorage
   * @param code grant code passed by sso issuer
   */
  public fetchAccessToken(code: string): Observable<boolean> {
    const { auth: { URLS, clientId, clientSecret }} = environment;
    const encodedData = window.btoa(`${clientId}:${clientSecret}`);
    const headers = new HttpHeaders().set('Authorization', `Basic ${encodedData}`);

    return this.http.post(`${URLS.base}${URLS.token}`, { grant_type: 'authorization_code', code }, { headers })
      .pipe(
        tap((res: { token: string }) => localStorage.setItem(LocalStorageConfig.ACCESS_TOKEN, res.token)),
        map(res => true)
      );
  }

  /**
   * get the logged in user details based on token from local storage
   * if successful, stores the user in lcoal storage
   */
  public getMe(): Observable<boolean> {
    const accessToken = this.getAccessToken();
    if (!accessToken) {
      this.loginRedirect();
      return of(false);
    }

    const { auth: { URLS }} = environment;
    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);
    return this.http
      .get(`${URLS.base}${URLS.me}`, { headers })
      .pipe(
        map((res: any) => {
          const matchedUser = environment.users.find(u => u.userName === res.userName);
          const user = Object.assign(
            {},
            res,
            matchedUser ? matchedUser : {},
            {
              avatar: matchedUser ? AvatarConfig[matchedUser.role] : null,
              username: `${res.name.givenName} ${res.name.familyName}`
            }
          );
          this.currentUser = user;
          return true;
        }),
        catchError(() => {
          this.loginRedirect();
          return of(false);
        })
      );
  }

  /**
   * redirect user to the sso login page
   */
  public handleSSOLogin() {
    const { auth: { URLS, clientId, redirectURI }} = environment;
    const ssoURL = `${URLS.base}${URLS.login}?client_id=${clientId}&response_type=code&redirect_uri=${redirectURI}`;
    window.location.href = ssoURL;
  }

  /**
   * get access token from storage
   */
  public getAccessToken(): string {
    return localStorage.getItem(LocalStorageConfig.ACCESS_TOKEN);
  }

  /**
   * clears local storage of all saved data
   */
  private clearLocalStorage() {
    Object.keys(LocalStorageConfig).forEach(key => {
      localStorage.removeItem(LocalStorageConfig[key]);
    });
  }

  /**
   * handles logout
   * deletes all data from local storage, calls sso logout
   * navigate to login
   */
  public logout() {
    const { auth: { URLS, logoutRedirectURI }} = environment;
    const accessToken = this.getAccessToken();
    const headers = new HttpHeaders().set('Authorization', `Bearer ${accessToken}`);
    this.http.post(`${URLS.base}${URLS.logout}`, { post_logout_redirect_uri: logoutRedirectURI }, { headers }).subscribe();
    this.loginRedirect();
  }


  /**
   * redirect to login page and delete all data from local storage
   */
  public loginRedirect() {
    this.clearLocalStorage();
    this.router.navigate(['/login']);
  }

  /**
   * gets the current logged in user
   */
  public getCurrentUser(): User {
    return this.currentUser;
  }

  /**
   * gets the role of the current logged in user
   */
  public getRole(): string {
    const user = this.getCurrentUser();
    return user ? user.role : null;
  }

}
