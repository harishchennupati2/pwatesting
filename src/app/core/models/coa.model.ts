import { Address } from './address.model';
import { Order } from './order.model';

export class Coa {
  linearId: string;
  hporderno: string;
  customerPoNumber: string;
  customersigned: string;
  customersigneddate: string | Date;
  signatoryposition: string;
  orders: Order[];
  customerprintedname: string;
  supplieraddress: Address;
  shiptoaddress: Address;
}
