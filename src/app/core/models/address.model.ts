export class Address {
  id: string;
  name: string;
  addressLine1: string;
  addressLine2: string;
  street: string;
  city: string;
  region: string;
  postalCode: string;
  country: string;
  attentionTo: string;
  telephone: string;
  faxNo: string;
  emailAddress: string;

}
