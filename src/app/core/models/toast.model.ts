export class Toast {
  message: string;
  color?: string; // themed color name
  delay?: number; // milliseconds
}
