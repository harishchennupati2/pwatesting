export class Column {
  name: string;
  key?: string;
  type?: string;
  category?: string;
  hidden?: boolean;
  style?: string;
  unsortable?: string;
  editable?: (arg0: any) => boolean;
}
