export enum UserRole {
  EndCustomer = 'EndCustomer',
  LeasingPartner = 'LeasingPartner',
  SupplyChainTeam = 'SupplyChainTeam',
  ManufacturingUnit = 'ManufacturingUnit',
  InstallationPartner = 'InstallationPartner'
}
