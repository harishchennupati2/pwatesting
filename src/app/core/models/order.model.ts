import { OrderItem } from './order-item.model';
import { Address } from './address.model';

export class Order {
  linearId: string;
  os: string;
  orderNo: string;
  salesOrderNo: string;
  orderType: string;
  orderTypeDescription: string;
  purchaseOrderDate: string | Date;
  purchaseAgreementNo: string;
  customerBaseNo: string;
  customerName: string;
  purchaseOrderNumber: string;
  quoteNumber: string;
  summarizedStatus: string;
  invoiceStatus: string;
  hpSalesRep: string;
  custPurchaseAgentName: string;
  custPurchaseAgentEmail: string;
  isLeasing: boolean;
  items: OrderItem[];
  soldTo: Address;
  shipTo: Address;
  invoiceTo: Address;
}
