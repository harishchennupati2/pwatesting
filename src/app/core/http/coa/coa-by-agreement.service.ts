import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CoaByAgreement } from '../../models/coa-by-agreement.model';
import { UserRole } from '../../models/user-role.model';
import { ApiService } from '../api.service';

import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CoaByAgreementService {

  constructor(private api: ApiService, private authService: AuthenticationService) { }

  /**
   * get paginated coabyagreements
   * @param params the query params
   * @param suffix the path suffix to add
   */
  public get(params?: any, suffix?: string): Observable<{ total: number, pageItems: CoaByAgreement[] }> {
    const defaultParams: any = { pageNumber: 1, pageSize: 10 };
    params = Object.assign({}, defaultParams, params);

    // if role is EndCustomer customerName param is mandatory
    const role = this.authService.getRole();
    if (role === UserRole.EndCustomer) {
      const user = this.authService.getCurrentUser();
      params.customerName = user.customerorgname;
    }

    // optional suffix
    suffix = (suffix ? `/${suffix}` : '').toLowerCase().replace(/\s*/g, '');
    return this.api
      .get(`coabyagreements${suffix}`, { params, observe: 'response' })
      .pipe(map(res => {
        return {
          total: res.headers.get('X-Total-Count'),
          pageItems: res.body
        };
      }));
  }

  public getByStatus(status: string, params?: any): Observable<{ total: number, pageItems: CoaByAgreement[] }> {
    return this.get(params, status);
  }

  public getById(id: string): Observable<CoaByAgreement> {
    return this.api.get(`coabyagreements/${id}`);
  }

  /**
   * sign coa by agreement PUT request
   */
  public sign(id: string, printname: string, title: string) {
    return this.api.put(`coabyagreements/${id}/sign`, { printname, title });
  }

  /**
   * create / edit coabyagreeemnt PUT request
   * @param coabya the coa by agreement object
   */
  public create(coabya: CoaByAgreement): Observable<CoaByAgreement> {
    coabya = Object.assign({}, coabya);
    (coabya as any).coas = coabya.coas.map(coa => ({ linearId: coa.linearId }));
    return this.api.post('coabyagreements', coabya);
  }

}
