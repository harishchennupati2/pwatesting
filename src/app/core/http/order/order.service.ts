import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Order } from '../../models/order.model';
import { UserRole } from '../../models/user-role.model';
import { ApiService } from '../api.service';

import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private api: ApiService, private authService: AuthenticationService) { }

  /**
   * get paginated orders
   * @param params the query params
   * @param suffix the path suffix to add
   */
  public get(params?: any): Observable<{ total: number, pageItems: Order[] }> {
    const defaultParams: any = { pageNumber: 1, pageSize: 10 };
    params = Object.assign({}, defaultParams, params);

    // if role is EndCustomer, customerName param is mandatory
    const role = this.authService.getRole();
    if (role === UserRole.EndCustomer) {
      const user = this.authService.getCurrentUser();
      params.customerName = user.customerorgname;
    }

    return this.api
      .get('orders', { params, observe: 'response' })
      .pipe(map(res => {
        return {
          total: res.headers.get('X-Total-Count'),
          pageItems: res.body
        };
      }));
  }

  public getByStatus(status: string, params?: any): Observable<{ total: number, pageItems: Order[] }> {
    params = Object.assign({}, params, { summarizedStatus: status });
    return this.get(params);
  }

  public getByOrderNo(orderNo: string): Observable<Order> {
    return this.api.get(`orders/${orderNo}`);
  }
  /**
   * get paginated signed orders
   * @param params the query params
   */
  public getSignedOrders(params?: any): Observable<{ total: number, pageItems: Order[] }> {
    const defaultParams: any = { pageNumber: 1, pageSize: 10 };
    params = Object.assign({}, defaultParams, params);
    return this.api
      .get('orders/items/devices/signed', { params, observe: 'response' })
      .pipe(map(res => {
        return {
          total: res.headers.get('X-Total-Count'),
          pageItems: res.body
        };
      }));
  }

  /**
   * update devices for orders
   * @param orders orders whose devices need to be updated
   */
  public updateOrderDevices(orders: any[]) {
    return this.api.put('orders/items/devices', { orders });
  }

}
