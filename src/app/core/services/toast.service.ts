import { Injectable } from '@angular/core';

import { Toast } from '../models/toast.model';

/**
 * toast service
 * general toast utility service
 * ToastComponent watches the public toasts array here
 */
@Injectable({
  providedIn: 'root'
})
export class ToastService {

  public toasts: Toast[] = [];

  constructor() { }

  /**
   * show toast
   * simply adds the toast to the public toast-list
   */
  public show(toast: Toast) {
    const index = this.toasts.indexOf(toast);
    if (index !== -1) { return; }
    this.toasts.push(toast);
  }

  /**
   * remove toast
   * simply remove the toast from the public toast-list
   */
  public remove(toast: Toast) {
    const index = this.toasts.indexOf(toast);
    if (index === -1) { return; }
    this.toasts.splice(index, 1);
  }
}
