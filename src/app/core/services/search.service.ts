import { Injectable } from '@angular/core';

/**
 * search service
 * NOTE: This is not a backend search, but frontend search
 */
@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() { }

  /**
   * filter a list of data objects based on search text
   * @param data - the data or object list
   * @param searchText - the text to search for
   */
  public filterItems(data: any[], searchText: string): any[] {
    if (!data) { return null; }
    return data.filter(obj => this.findMatchInObject(obj, searchText));
  }

  /**
   * handy fn to tell whether an object has the desired regex in it (however deep the object is)
   * this is done by stringifying the object, and then removing all the keynames from the JSON string
   * thus the regex matches only the values of the object
   * @param obj - the object in which to search
   * @param searchText - the text to search for
   * @returns boolean - whether a match was found in object or not
   */
  private findMatchInObject(obj: any, searchText: string): boolean {
    if (!searchText) { return true; }
    const regex = new RegExp(searchText, 'gi');

    let json = JSON.stringify(obj);
    // replace the 'key-names' / 'property-names' from the JSON string
    json = json.replace(/\"[^\"]*\"\:/g, '');
    return !!json.match(regex);
  }
}
