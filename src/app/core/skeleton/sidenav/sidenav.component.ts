import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

class NavItem { label: string; icon: string; link: string; active?: boolean; }

import { UserRole } from 'src/app/core/models/user-role.model';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  public navItems: NavItem[] = [
    { label: 'DASHBOARD', icon: 'dashboard', link: 'dashboard' },
    { label: 'ORDERS', icon: 'assignment', link: 'order-listing' },
    { label: 'COAs', icon: 'description', link: 'coas/coa-listing' },
    { label: 'COA BY AGREEMENTS', icon: 'assignment_turned_in', link: 'coas/coa-by-agreement-listing' },
    { label: 'INVOICES', icon: 'receipt', link: 'invoice-listing' }
  ];

  constructor(private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
    this.markActiveNavItem();

    const role = this.authService.getRole();
    if (role === UserRole.InstallationPartner || role === UserRole.ManufacturingUnit) {
      this.navItems.splice(3);
    }

    this.router.events.subscribe(route => {
      if (route instanceof NavigationEnd) {
        this.markActiveNavItem();
      }
    });
  }

  /**
   * mark nav item
   * mark all items as inactive first, and then activate item based on provided route
   */
  private markActiveNavItem() {
    this.navItems.forEach(item => {
      item.active = false;
    });
    const navItem = this.navItems.filter(item => this.router.url.indexOf(item.link) !== -1)[0];
    if (navItem) {
      navItem.active = true;
    }
  }

  public navItemClicked(item: NavItem) {
    this.router.navigate([item.link]);
  }

}
