import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { MiscellaneousService } from 'src/app/core/http/miscellaneous/miscellaneous.service';
import { User } from 'src/app/core/models/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public searchText: string;
  public searchTextUpdate = new Subject<void>();
  public showSearchResults: boolean;
  public searchResults: any[];
  public user: User;
  public path: string[];

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private miscService: MiscellaneousService
  ) { }

  ngOnInit() {
    this.user = this.authService.getCurrentUser();

    this.extractPath();
    this.router.events.subscribe(route => {
      if (route instanceof NavigationEnd) {
        this.extractPath();
      }
    });

    // set up the debounced search flow (used by project-details mode)
    this.searchTextUpdate
      .pipe(debounceTime(250))
      .subscribe(() => {
        this.search();
      });
  }

  /**
   * extracts the human readable route path
   * i.e. converts 'first/second-part/third' to 'First / Second Part / Third'
   */
  private extractPath() {
    // helper method to capitalize all words in a string
    const capitalize = (route: string, separator: string) => {
      route = route.replace('coa', 'COA');
      return route
        .split(separator)
        .map(part => part.charAt(0).toUpperCase() + part.slice(1))
        .join(separator);
    };

    // figure out based on route what module user is in
    const topLevelPaths = ['dashboard', 'order-listing', 'coa-listing', 'coa-by-agreement-listing', 'invoice-listing'];
    const url = this.router.url;
    const currentTop = topLevelPaths.filter(t => url.indexOf(t) !== -1)[0];

    // compile the final path strings
    let path = capitalize(url.substring(url.indexOf(currentTop)), '/');
    path = capitalize(path, '-');
    path = path.replace(/\-/g, ' ');
    this.path = path.split('/').slice(0, 2);
  }

  /**
   * logout
   * auth service handles route change to login page internally
   */
  public logout() {
    this.authService.logout();
  }

  /**
   * search (debounced)
   * calls the search API for resource IDs
   */
  private search() {
    if (!this.searchText) { return this.searchResults = null; }

    // backend returns results in the form of entities (or resources)
    // this map adds some helpful metadata for UI
    const entityMap = {
      orders: { name: 'Orders', link: '/order-listing/details' },
      coas: { name: 'COA ID', link: '/coas/coa-listing/details' },
      coabyagreements: { name: 'COA by Agreement ID', link: '/coas/coa-by-agreement-listing/details' },
      invoices: { name: 'Invoices', link: '/invoice-listing/details' }
    };
    this.miscService.search(this.searchText).subscribe(res => {
      this.searchResults = res;
      res.forEach(e => e.entity = entityMap[e.entityType]);
    });
  }

  /**
   * navigate to
   * @param entity the resource type
   * @param id the specific resource id
   */
  public navigateTo(entity: { link: string }, id: string) {
    this.router.navigate([entity.link + '/' + id]);
    this.showSearchResults = false;
  }

}
