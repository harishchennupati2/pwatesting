import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService as AuthGuard } from 'src/app/core/authentication/auth-guard.service';
import { UserRole } from 'src/app/core/models/user-role.model';

import { SkeletonComponent } from './core/skeleton/skeleton.component';

// lazy loaded routes
const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./modules/login/login.module').then(mod => mod.LoginModule)
  },
  // all authenticated routes
  {
    path: '',
    component: SkeletonComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(mod => mod.DashboardModule),
      },
      {
        path: 'order-listing',
        loadChildren: () => import('./modules/orders/orders.module').then(mod => mod.OrdersModule),
      },
      {
        path: 'coas',
        loadChildren: () => import('./modules/coas/coas.module').then(mod => mod.CoasModule),
      },
      {
        path: 'invoice-listing',
        loadChildren: () => import('./modules/invoices/invoices.module').then(mod => mod.InvoicesModule),
        data: { roles: [UserRole.EndCustomer, UserRole.LeasingPartner, UserRole.SupplyChainTeam] }
      },
      {
        path: '**',
        redirectTo: 'dashboard'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'login',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
