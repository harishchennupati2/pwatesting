import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

import { UserRole } from 'src/app/core/models/user-role.model';
import { Column } from 'src/app/core/models/column.model';
import { Invoice } from 'src/app/core/models/invoice.model';
import { InvoiceService } from 'src/app/core/http/invoice/invoice.service';
import { MiscellaneousService } from 'src/app/core/http/miscellaneous/miscellaneous.service';

@Component({
  selector: 'app-invoice-listing',
  templateUrl: './invoice-listing.component.html',
  styleUrls: ['./invoice-listing.component.scss']
})
export class InvoiceListingComponent implements OnInit {

  public statusCounts: { value: number, label: string }[];
  public statuses = ['Active', 'Draft'];
  public columns = [
    { name: 'Invoice ID', key: 'invoiceNo',  type: 'link' },
    { name: 'Invoice Date', key: 'invoiceDate', type: 'date' },
    { name: 'PO Number', key: 'poNumber' },
    { name: 'Total Amount', key: 'totalAmount', type: 'currency' },
    { name: 'Summarized Status', key: 'status', type: 'status', icon: 'cloud_download' }
  ] as Column[];

  constructor(
    private miscService: MiscellaneousService,
    private router: Router,
    public invoiceService: InvoiceService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    const role = this.authService.getRole();
    this.columns[this.columns.length - 1].editable = (item: Invoice) =>  {
      return role === UserRole.SupplyChainTeam && item.status === 'DRAFT';
    };

    this.miscService.getStatuses().subscribe(res => {
      this.statusCounts = res.invoice;
    });
  }

  public navigateToInvoice(invoice: Invoice) {
    this.router.navigate(['/invoice-listing/details', invoice.invoiceNo]);
  }

  public createInvoice(invoice: Invoice) {
    this.router.navigate(['/invoice-listing/create-new', invoice.invoiceNo]);
  }

}
