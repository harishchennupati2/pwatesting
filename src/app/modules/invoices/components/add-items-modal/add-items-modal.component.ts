import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Column } from 'src/app/core/models/column.model';
import { InvoiceItem } from 'src/app/core/models/invoice-item.model';
import { InvoiceItemService } from 'src/app/core/http/invoice/invoice-item.service';

@Component({
  selector: 'app-add-items-modal',
  templateUrl: './add-items-modal.component.html',
  styleUrls: ['./add-items-modal.component.scss']
})
export class AddItemsModalComponent implements OnInit {

  constructor(public invoiceItemService: InvoiceItemService, public modal: NgbActiveModal) { }

  public selectedItems: InvoiceItem[];

  public columns = [
    { name: '', type: 'select', unsortable: true },
    { name: 'Customer Item No', key: 'itemNumberCustomer' },
    { name: 'Product Description', key: 'productDescription' },
    { name: 'Product Number', key: 'productNumber' },
    { name: 'Serial Number', key: 'serialNo' },
    { name: 'Net Unit Price (GBP)', key: 'unitPrice', type: 'number' },
    { name: 'Tax Amount', key: 'taxAmount', type: 'percentage' },
    { name: 'Qty.', key: 'quantity', type: 'number' },
    { name: 'Total Net Price (GBP)', key: 'totalNetPrice', type: 'number' },
    { name: 'Net Price With Tax (GBP)', key: 'netPriceWithTax', type: 'number' }
  ] as Column[];

  ngOnInit() {
  }

  public addItems() {
    this.modal.close(this.selectedItems);
  }

}
