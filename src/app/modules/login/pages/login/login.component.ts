import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { finalize } from 'rxjs/operators';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loggingIn = false;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    const code = this.route.snapshot.queryParamMap.get('code');
    if (code) {
      this.loggingIn = true;
      this.authService.fetchAccessToken(code)
        .pipe(finalize(() => this.loggingIn = false))
        .subscribe(() => {
          // if successful, navigte to splash screen
          this.router.navigate(['/login/redirect']);
        });
    }
  }

  public login() {
    this.authService.handleSSOLogin();
  }

}
