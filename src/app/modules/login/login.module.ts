import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { SplashComponent } from './pages/splash/splash.component';


@NgModule({
  declarations: [LoginComponent, SplashComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    NgbModule,
    FormsModule,
    SharedModule
  ]
})
export class LoginModule { }
