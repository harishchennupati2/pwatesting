import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from 'src/app/shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CoasRoutingModule } from './coas-routing.module';
import { CoaListingComponent } from './pages/coa-listing/coa-listing.component';
import { CoaDetailsComponent } from './pages/coa-details/coa-details.component';
import { CustomerDeclarationComponent } from './components/customer-declaration/customer-declaration.component';
import { CoaByAgreementListingComponent } from './pages/coa-by-agreement-listing/coa-by-agreement-listing.component';
import { CoaByAgreementDetailsComponent } from './pages/coa-by-agreement-details/coa-by-agreement-details.component';
import { AddressesComponent } from './components/addresses/addresses.component';
import { CreateNewByAgreementComponent } from './pages/create-new-by-agreement/create-new-by-agreement.component';
import { LeaseTextsComponent } from './components/lease-texts/lease-texts.component';
import { CoaSimpleTableComponent } from './components/coa-simple-table/coa-simple-table.component';
import { AddCoasModalComponent } from './components/add-coas-modal/add-coas-modal.component';


@NgModule({
  declarations: [
    CoaListingComponent,
    CoaDetailsComponent,
    CustomerDeclarationComponent,
    CoaByAgreementListingComponent,
    CoaByAgreementDetailsComponent,
    AddressesComponent,
    CreateNewByAgreementComponent,
    LeaseTextsComponent,
    CoaSimpleTableComponent,
    AddCoasModalComponent,
  ],
  imports: [
    CommonModule,
    CoasRoutingModule,
    SharedModule,
    FormsModule,
    NgbModule
  ],
  entryComponents: [
    AddCoasModalComponent
  ]
})
export class CoasModule { }
