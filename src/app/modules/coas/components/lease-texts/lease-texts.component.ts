import { Component, OnInit, Input, Output } from '@angular/core';

import { ClientConfig } from 'src/app/configs/client.config';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-lease-texts',
  templateUrl: './lease-texts.component.html',
  styleUrls: ['./lease-texts.component.scss']
})
export class LeaseTextsComponent implements OnInit {

  @Input() canEdit = false;
  @Input() equipmentLocation = '';

  public leaseTexts: {
    masterLease: string;
    leaseAcceptance: string;
    financingAcceptance: string;
    leaseAcknowledgements: string;
  };

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.compileLeaseTexts();
  }

  private compileLeaseTexts() {
    const replaceVariables = (text: string) => {
      text = text.replace('<<CLIENT_NAME>>', ClientConfig.NAME);
      return text.replace('<<CUSTOMER_ORG_NAME>>', this.authService.getCurrentUser().customerorgname);
    };
    this.leaseTexts = {
      masterLease: replaceVariables(ClientConfig.MASTER_LEASE_AGREEMENT),
      leaseAcceptance: replaceVariables(ClientConfig.LEASE_ACCEPTANCE),
      financingAcceptance: replaceVariables(ClientConfig.FINANCING_ACCEPTANCE),
      leaseAcknowledgements: replaceVariables(ClientConfig.LEASE_ACKNOWLEDGEMENTS)
    };
  }

}
