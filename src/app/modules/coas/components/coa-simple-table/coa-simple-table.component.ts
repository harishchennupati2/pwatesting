import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Coa } from 'src/app/core/models/coa.model';
import { Column } from 'src/app/core/models/column.model';

@Component({
  selector: 'app-coa-simple-table',
  templateUrl: './coa-simple-table.component.html',
  styleUrls: ['./coa-simple-table.component.scss']
})
export class CoaSimpleTableComponent implements OnInit {

  @Input() coas: Coa[];
  @Input() deletable: boolean;
  @Output() delete = new EventEmitter<Coa>();

  public columns = [
    { name: 'Serial No.', key: 'serialNumber' },
    { name: 'Acceptance ID', key: 'acceptanceId' },
    { name: 'Customer PO No.', key: 'customerPoNumber' },
    { name: 'Customer Signed', key: 'customersigned' },
    { name: 'Customer Signed Date', key: 'customersigneddate', type: 'date' },
    { name: 'Customer Printed Name', key: 'customerprintedname' },
    { name: 'Signatory Position', key: 'signatoryposition' }
  ] as Column[];

  constructor() { }

  ngOnInit() {
    if (this.deletable) {
      this.columns.push({ name: '', type: 'delete', style: 'delete' });
    }
  }

  public onDelete(coa: Coa) {
    this.delete.emit(coa);
  }

}
