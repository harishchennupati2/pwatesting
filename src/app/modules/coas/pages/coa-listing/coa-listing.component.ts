import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserRole } from 'src/app/core/models/user-role.model';
import { Column } from 'src/app/core/models/column.model';
import { Coa } from 'src/app/core/models/coa.model';
import { CoaService } from 'src/app/core/http/coa/coa.service';
import { MiscellaneousService } from 'src/app/core/http/miscellaneous/miscellaneous.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Component({
  selector: 'app-coa-listing',
  templateUrl: './coa-listing.component.html',
  styleUrls: ['./coa-listing.component.scss']
})
export class CoaListingComponent implements OnInit {

  public statusCounts: { value: number, label: string }[];
  public statuses = ['Signed', 'Not Signed'];
  public columns = [
    { name: 'COA ID', key: 'linearId',  type: 'link' },
    { name: 'Customer PO No.', key: 'customerPoNumber' },
    { name: 'Install Date', key: 'installdate' },
    { name: 'Signed', key: 'customersigned' },
    { name: 'Signed Date', key: 'customersigneddate', type: 'date' },
    { name: 'Signatory Position', key: 'signatoryposition' },
    { name: 'Summarized Status', key: 'customersigned', type: 'status' }
  ] as Column[];

  constructor(
    private miscService: MiscellaneousService,
    private router: Router,
    public coaService: CoaService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    const role = this.authService.getRole();
    this.columns[this.columns.length - 1].editable = (item: Coa) =>  {
      return role === UserRole.EndCustomer && item.customersigned === 'no';
    };

    this.miscService.getStatuses().subscribe(res => {
      this.statusCounts = res.coa;
    });
  }

  public navigateToCoa(coa: Coa) {
    this.router.navigate(['/coas/coa-listing/details', coa.linearId]);
  }

}
