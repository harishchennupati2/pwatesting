import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs';
import { uniq, get } from 'lodash';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { ToastService } from 'src/app/core/services/toast.service';

import { UserRole } from 'src/app/core/models/user-role.model';
import { CoaByAgreement } from 'src/app/core/models/coa-by-agreement.model';
import { Coa } from 'src/app/core/models/coa.model';
import { CoaByAgreementService } from 'src/app/core/http/coa/coa-by-agreement.service';

@Component({
  selector: 'app-coa-by-agreement-details',
  templateUrl: './coa-by-agreement-details.component.html',
  styleUrls: ['./coa-by-agreement-details.component.scss']
})
export class CoaByAgreementDetailsComponent implements OnInit, OnDestroy {

  private routeSubscription: Subscription;
  public coabya: CoaByAgreement;
  public statuses = ['Not Signed', 'Signed'];
  public customerOrgName: string;

  public orderNumbers: string[] = [];
  public filteredCoas: Coa[];

  public signing: boolean;
  public userAllowedToSign: boolean;

  constructor(
    private coabyaService: CoaByAgreementService,
    private route: ActivatedRoute,
    private location: Location,
    private authService: AuthenticationService,
    private toastService: ToastService
  ) { }

  ngOnInit() {
    this.userAllowedToSign = this.authService.getRole() === UserRole.EndCustomer;
    this.customerOrgName = this.authService.getCurrentUser().customerorgname;

    this.routeSubscription = this.route.params.subscribe(params => {
      this.coabyaService.getById(params.id).subscribe(coabya => {
        this.coabya = coabya;
        if (this.coabya.coas && this.coabya.coas.length) {
          this.orderNumbers = this.coabya.coas.reduce((accu, coa) => {
            if (!coa.orders || !coa.orders.length) {
              return [];
            }
            return accu.concat(coa.orders.map(order => order.orderNo));
          }, []);

          this.orderNumbers = uniq(this.orderNumbers);

          // serial number of coa will be serial number of any device in that coa
          this.coabya.coas = this.coabya.coas.map(coa => ({
            ...coa,
            serialNumber: get(coa, 'orders[0].items[0].devices[0].serialNumber', '')
          }));
        }
      });
    });
  }

  ngOnDestroy() {
    this.routeSubscription.unsubscribe();
  }

  public goBack() {
    this.location.back();
  }

  public onSignInit() {
    const user = this.authService.getCurrentUser();
    this.coabya.printname = user.username;
    this.coabya.title = user.signatoryposition;
    this.coabya.acceptancedate = new Date();
  }

  public sign() {
    this.signing = true;
    this.coabyaService
      .sign(this.coabya.linearId, this.coabya.printname, this.coabya.title)
      .subscribe(coabya => {
        this.signing = false;
        this.coabya = coabya;
        this.toastService.show({ message: 'COA by Agreement has been signed!', color: 'success' });
      });
  }

}
