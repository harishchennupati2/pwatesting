import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderListingComponent } from './pages/order-listing/order-listing.component';
import { OrderDetailsComponent } from './pages/order-details/order-details.component';

const routes: Routes = [
  {
    path: '',
    component: OrderListingComponent
  },
  {
    path: 'details/:id',
    component: OrderDetailsComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
