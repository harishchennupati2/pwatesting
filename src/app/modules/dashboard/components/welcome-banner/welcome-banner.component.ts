import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { UtilService } from 'src/app/core/services/util.service';
import { User } from 'src/app/core/models/user.model';

@Component({
  selector: 'app-welcome-banner',
  templateUrl: './welcome-banner.component.html',
  styleUrls: ['./welcome-banner.component.scss']
})
export class WelcomeBannerComponent implements OnInit {

  public user: User;
  public timeOfDay: string;
  public hideBanner: boolean;

  constructor(private authService: AuthenticationService, private utilService: UtilService) { }

  ngOnInit() {
    this.user = this.authService.getCurrentUser();
    this.timeOfDay = this.utilService.getTimeOfDay();
  }

}
