import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { Column } from 'src/app/core/models/column.model';
import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-summary-card',
  templateUrl: './summary-card.component.html',
  styleUrls: ['./summary-card.component.scss']
})
export class SummaryCardComponent implements OnChanges {

  @Input() name: string;
  @Input() columns: Column;
  @Input() data: any[];
  @Input() statuses: { value: number, label: string, color?: string }[];
  @Output() navigate = new EventEmitter<any>();

  constructor(public utilService: UtilService) { }

  ngOnChanges() {
    if (!this.statuses) { return; }
    this.statuses.forEach(status => {
      status.color = this.utilService.getColorForStatus(status.label) || 'background-strong';
    });
  }

  public goTo(id?: string) {
    this.navigate.emit(id);
  }

}
