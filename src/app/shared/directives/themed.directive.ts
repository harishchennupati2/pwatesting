import { Directive, ElementRef, Renderer2 } from '@angular/core';

import { ThemesConfig } from 'src/app/configs/themes.config';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Directive({
  selector: '[appThemed]'
})
export class ThemedDirective {

  constructor(
    private elemRef: ElementRef,
    private renderer: Renderer2,
    private authService: AuthenticationService
  ) {
    const user = this.authService.getCurrentUser();
    const theme = 'theme-' + (user ? (ThemesConfig[user.customerorgname] || 'default') : 'default');
    this.renderer.addClass(this.elemRef.nativeElement, theme);
  }

}
