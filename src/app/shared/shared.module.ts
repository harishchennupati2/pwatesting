import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ClickOutsideModule } from 'ng-click-outside';

import { ThemedDirective } from './directives/themed.directive';
import { DoughnutChartComponent } from './components/doughnut-chart/doughnut-chart.component';
import { StatusBannerComponent } from './components/status-banner/status-banner.component';
import { ArcChartComponent } from './components/arc-chart/arc-chart.component';
import { ListingTableComponent } from './components/listing-table/listing-table.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { StatusFilterGroupComponent } from './components/status-filter-group/status-filter-group.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { DetailsPanelComponent } from './components/details-panel/details-panel.component';
import { ToggleComponent } from './components/toggle/toggle.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { SelectComponent } from './components/select/select.component';

@NgModule({
  declarations: [
    ThemedDirective,
    DoughnutChartComponent,
    StatusBannerComponent,
    ArcChartComponent,
    ListingTableComponent,
    SearchBoxComponent,
    StatusFilterGroupComponent,
    PaginatorComponent,
    DetailsPanelComponent,
    ToggleComponent,
    CheckboxComponent,
    SelectComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ClickOutsideModule
  ],
  exports: [
    ThemedDirective,
    DoughnutChartComponent,
    StatusBannerComponent,
    ArcChartComponent,
    ListingTableComponent,
    SearchBoxComponent,
    StatusFilterGroupComponent,
    PaginatorComponent,
    DetailsPanelComponent,
    ToggleComponent,
    CheckboxComponent,
    SelectComponent
  ]
})
export class SharedModule { }
