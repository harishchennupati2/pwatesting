import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Column } from 'src/app/core/models/column.model';
import { UtilService } from 'src/app/core/services/util.service';
import { ApiErrorResponse } from 'src/app/core/models/api-error-response.model';

/**
 * listing table component
 * generalized pagination and search table
 * it accepts a Service as Input() param to fetch the paginated data from
 */
@Component({
  selector: 'app-listing-table',
  templateUrl: './listing-table.component.html',
  styleUrls: ['./listing-table.component.scss']
})
export class ListingTableComponent implements OnInit {

  @Input() columns: Column[];
  @Input() statuses: string[];
  @Input() service: any; // the api service from which to fetch data
  @Input() apiPathSuffix: string; // optional api path suffix
  @Input() title: string;
  @Input() styling: string; // use 'simple' to change the default styling to match other tables in app
  @Output() linkClick = new EventEmitter<any>();
  @Output() edit = new EventEmitter<any>(); // edit icon clicked
  @Output() select = new EventEmitter<any[]>(); // checkbox selected
  public data: any[];
  public total: number;
  public pageNumber = 1;
  public pageSize = 10;
  public loading: boolean;
  public sortBy: Column;
  public sortOrder: string;
  public statusFilterValue: string;
  private searchText: string;
  public selectedItems: any[] = [];

  constructor(public utilService: UtilService) { }

  ngOnInit() {
    this.fetchPageData();
  }

  /**
   * fetch page data
   * the meat and bones of this component
   */
  private fetchPageData() {
    this.loading = true;

    // compile the params to send to apiService
    const params = {
      pageNumber: this.pageNumber,
      pageSize: this.pageSize,
      sortDirection: this.sortOrder,
      sortColumn: this.sortBy && this.sortBy.key,
      search: this.searchText
    };

    // call the 'get' method of the given service
    this.service.get(params, this.apiPathSuffix).subscribe((res: { total: number, pageItems: any[] }) => {
      this.total = res.total;
      this.data = res.pageItems;
      this.loading = false;
    }, (err: ApiErrorResponse) => {
      if (err.status === 404) {
        // if search criteria not met by any rows in backend DB, 404 is returned by MOCK API
        this.data = [];
        this.total = 0;
        this.loading = false;
      }
    });
  }

  public onPaginatorUpdate() {
    this.fetchPageData();
  }

  public onSortBy(column: Column) {
    if (this.sortBy !== column) {
      this.sortOrder = null;
    }
    this.sortBy = column;
    this.sortOrder = this.sortOrder === 'asc' ? 'desc' : 'asc';
    this.fetchPageData();
  }

  public onFilterUpdated() {
    this.pageNumber = 1;
    this.fetchPageData();
  }

  public onSearch(text: string) {
    this.searchText = text;
    this.fetchPageData();
  }

  public onLinkClicked(row: any) {
    this.linkClick.emit(row);
  }

  public isSelected(row: any) {
    return this.selectedItems.filter(item => JSON.stringify(item) === JSON.stringify(row)).length;
  }

  /**
   * on select
   * @param row - the row being selected / de-selected
   * @param value - true or false
   * selected items are stored in a separate array
   * so as to preserve selected state on page change
   */
  public onSelect(row: any, value: boolean) {
    if (value && !this.isSelected(row)) {
      this.selectedItems.push(row);
    } else if (!value) {
      this.selectedItems.splice(
        // simple object equality won't do, need to check if objects are the same by value (as opposed to reference)
        this.selectedItems.map(item => JSON.stringify(item)).indexOf(JSON.stringify(row))
      , 1);
    }
    this.select.emit(this.selectedItems);
  }

  public onEdit(row: any) {
    this.edit.emit(row);
  }

}
