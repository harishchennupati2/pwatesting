import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * select component
 * i.e. dropdown picker
 */
@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent {

  public active = false;
  public selectedOption: any;
  @Input() options: any[];
  @Input() placeholder: string;
  // if the select can be reset
  @Input() canReset = true;
  @Input() position: 'top' | 'bottom' = 'bottom';
  // similar to ngModel, define a 2 way binding for selected option
  @Input()
  get appModel() {
    return this.selectedOption;
  }
  set appModel(val: any) {
    this.selectedOption = val;
  }
  @Output() appModelChange = new EventEmitter<any>();
  @Output() change = new EventEmitter<void>();

  constructor() { }

  /**
   * select option
   * deactivate the select box and notify parent
   */
  public selectOption(option: any) {
    this.active = false;

    if (this.selectedOption !== option) {
      this.selectedOption = option;
      this.appModelChange.emit(option);
      this.change.emit();
    }
  }

}
