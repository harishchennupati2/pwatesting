import { UserRole } from '../core/models/user-role.model';

// static avatar images are mapped to user role
export const AvatarConfig = {
  [UserRole.EndCustomer]: '/assets/images/avatar-end-customer.png',
  [UserRole.LeasingPartner]: '/assets/images/avatar-leasing-partner.png',
  [UserRole.ManufacturingUnit]: '/assets/images/avatar-manufacturing-unit.png',
  [UserRole.SupplyChainTeam]: '/assets/images/avatar-supply-chain-team.png',
  [UserRole.InstallationPartner]: '/assets/images/avatar-installation-partner.png'
};
