// the key names for localstorage
export const LocalStorageConfig = {
  ACCESS_TOKEN: 'access_token',
  CURRENT_USER: 'current_user',
  ORDER_ITEM_COLUMN_SETTINGS: 'order_item_column_settings'
};
